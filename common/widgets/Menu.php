<?php
namespace common\widgets;

use frontend\models\Menu as modelMenu;

class Menu{

    private static function getMenu($id){
        return modelMenu::getItems($id);
    }

    public static function render($id){

        $menuObject = self::getMenu($id);

        $tpl = '<script> var topMenu = ' .  json_encode($menuObject) . '</script>';

        return $tpl;
    }

}