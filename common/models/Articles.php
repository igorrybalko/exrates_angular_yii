<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $meta_tags
 * @property string $meta_description
 * @property int $published
 * @property string $created
 * @property string $seo_title
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['meta_tags', 'meta_description'], 'string'],
            [['published'], 'integer'],
            [['created'], 'safe'],
            [['title', 'slug', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'meta_tags' => Yii::t('app', 'Meta Tags'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'published' => Yii::t('app', 'Published'),
            'created' => Yii::t('app', 'Created'),
            'seo_title' => Yii::t('app', 'Seo Title'),
        ];
    }
}
