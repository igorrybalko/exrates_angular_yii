<?php

use yii\db\Migration;

/**
 * Handles adding seo_title to table `articles`.
 */
class m180412_082748_add_seo_title_column_to_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('articles', 'seo_title', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('articles', 'seo_title');
    }
}
