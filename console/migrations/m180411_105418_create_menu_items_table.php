<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_items`.
 */
class m180411_105418_create_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu_items', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'type' => $this->string(255),
            'menu' => $this->integer()->notNull(),
            'published' => $this->smallInteger(1)->defaultValue(0),
            'created' => $this->timestamp()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu_items');
    }
}
