<?php

use yii\db\Migration;

/**
 * Handles adding order to table `menu_items`.
 */
class m180411_120610_add_order_column_to_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu_items', 'order', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('menu_items', 'order');
    }
}
