<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m180408_192115_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'meta_tags' => $this->text(255),
            'meta_description' => $this->text(255),
            'published' => $this->smallInteger(1)->defaultValue(0),
            'created' => $this->timestamp()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }
}
