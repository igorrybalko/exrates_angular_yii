<?php

use yii\db\Migration;

/**
 * Handles adding url to table `menu_items`.
 */
class m180412_091755_add_url_column_to_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu_items', 'url', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('menu_items', 'url');
    }
}
