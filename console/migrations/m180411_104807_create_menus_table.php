<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menus`.
 */
class m180411_104807_create_menus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menus', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'published' => $this->smallInteger(1)->defaultValue(0),
            'created' => $this->timestamp()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menus');
    }
}
