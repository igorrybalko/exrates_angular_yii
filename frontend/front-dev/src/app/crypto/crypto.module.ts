import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CryptoComponent } from './crypto.component';
import {CryptoService} from "./crypto.service";
import {CryptoRoutingModule} from "./crypto-routing.module";
import {InputTextModule} from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CryptoRoutingModule,
    InputTextModule,
    FormsModule
  ],
  declarations: [CryptoComponent],
  providers: [
      CryptoService
  ]
})
export class CryptoModule { }
