import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styles: []
})
export class NotFoundComponent implements OnInit {

  documentTitle: string = 'Страница 404';

  constructor() { }

  ngOnInit() {

    document.title = this.documentTitle;
  }

}
