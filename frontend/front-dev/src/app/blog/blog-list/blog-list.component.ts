import { Component, OnInit } from '@angular/core';

import {BlogService} from "../blog.service";
import {NewsItem} from "../../shared/models/NewsItem";

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
})
export class BlogListComponent implements OnInit {

  documentTitle: string = 'Новости';

  constructor(
      private blogService: BlogService
  ) { }
  
  itemsBlog: NewsItem[];

  ngOnInit() {
    
    this.blogService.getItems('/api/news').subscribe((response: NewsItem[])=>{
      this.itemsBlog = response;
      document.title = this.documentTitle;

    });

	 // this.httpService.getXML('/assets/cache/news.xml').subscribe((response: any) => {
    //
    //      let parser = new DOMParser();
    //      let xmlDoc = parser.parseFromString(response,"text/xml");
    //
	 // 	console.log(xmlDoc.getElementsByTagName('item'));
    //
    // });

  }

}
