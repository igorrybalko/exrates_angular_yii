import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BlogListComponent} from "./blog-list/blog-list.component";
import {BlogItemComponent} from "./blog-item/blog-item.component";
import {BlogService} from "./blog.service";
import {BlogRoutingModule} from "./blog-routing.module";

@NgModule({
    imports: [
        CommonModule,
        BlogRoutingModule
    ],
    declarations: [
        BlogListComponent,
        BlogItemComponent
    ],
    providers: [
        BlogService
    ]
})
export class BlogModule { }