import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

import {BlogService} from "../blog.service";
import {NewsItem} from "../../shared/models/NewsItem";

@Component({
  selector: 'app-blog-item',
  templateUrl: './blog-item.component.html'
})
export class BlogItemComponent implements OnInit {

  itemBlog: NewsItem;
  id: number;

  constructor(private blogService: BlogService,
              private route: ActivatedRoute) {
    this.id = + route.snapshot.params["id"];
  }

  ngOnInit() {
    this.blogService.getItems('/api/news').subscribe((response: NewsItem[])=>{
      this.itemBlog = response[this.id];
      this.itemBlog.img = this.itemBlog.img.replace('190x120','1200x0');
      document.title = this.itemBlog.title;
    });
  }

}
