import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {DropdownModule} from 'primeng/dropdown';
import {SpinnerModule} from 'primeng/spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InputTextModule} from 'primeng/inputtext';
import {StoreModule} from '@ngrx/store';

import { AppComponent } from './app.component';

import {AppRoutingModule} from "./app-routing.module";
import {BlogModule} from "./blog/blog.module";

import {HttpService} from "./shared/services/http.service";
import {FooterComponent} from "./shared/components/footer/footer.component";
import {HeaderComponent} from "./shared/components/header/header.component";
import {HeaderMenuComponent} from "./shared/components/header/header-menu/header-menu.component";
import { ForexratesComponent } from './shared/components/forexrates/forexrates.component';
import {NbuService} from "./shared/services/nbu.service";
import {MenuService} from "./shared/services/menu.service";
import {MainConfig} from "./shared/config/mainConfig";
import {BankexchangeService} from './bankexchange/bankexchange.service';
import {BankexchangeComponent} from "./bankexchange/bankexchange.component";
import {ConverterComponent} from "./converter/converter.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {NbuComponent} from "./nbu/nbu.component";
import {nbuReducer} from "./redux/nbu.reducer";


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HeaderMenuComponent,
    ForexratesComponent,
    BankexchangeComponent,
    ConverterComponent,
    NotFoundComponent,
    NbuComponent
  ],
  providers: [
    HttpService,
    NbuService,
    MenuService,
    MainConfig,
    BankexchangeService
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BlogModule,
    DropdownModule,
    SpinnerModule,
    BrowserAnimationsModule,
    InputTextModule,
    StoreModule.forRoot({nbu: nbuReducer})
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
