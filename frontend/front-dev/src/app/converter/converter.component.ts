import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'

import {NbuService} from '../shared/services/nbu.service'
import {NbuCurrencyModel} from '../shared/models/NbuCurrencyModel'
import {AppState} from '../redux/app.state'

interface ConvForm {
    from: NbuCurrencyModel;
    to: NbuCurrencyModel;
    toValue: number;
    fromValue: number;
}

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html'
})
export class ConverterComponent implements OnInit {

  nbuCurrencies: NbuCurrencyModel[];

  convForm: ConvForm;

  documentTitle: string = 'Конвертер валют';

  constructor(private nbuService: NbuService, private store: Store<AppState>) {
      this.convForm = {
          from: new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'),
          to: new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'),
          toValue: 0,
          fromValue: 0
      }
  }

    ngOnInit() {

        this.getStoreData();
        document.title = this.documentTitle;

    }

    getStoreData() {
        this.store.select('nbu').subscribe(data => {
            if (!data.nbuCurrencies.length) {
                this.nbuService.loadRates().then((result: NbuCurrencyModel[]) => {
                    this.nbuCurrencies = result;
                    this.nbuCurrencies.push(new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'));
                });
            } else {
                this.nbuCurrencies = data.nbuCurrencies;
                this.nbuCurrencies.push(new NbuCurrencyModel(1, 'Украинская гривна', 1, 'UAH', '1'));
            }
        });
    }

  onChange(target = 'to'): void{

      let source = {
          to: 'from',
          from: 'to'
      }

      let result = (this.convForm.from.rate / this.convForm.to.rate) * this.convForm[source[target] + 'Value'];
      this.convForm[target + 'Value'] = Math.floor(result * 100) / 100;
  }

}