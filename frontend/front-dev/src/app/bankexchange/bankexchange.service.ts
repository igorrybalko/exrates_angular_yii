import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MainConfig} from '../shared/config/mainConfig';
import {Observable} from 'rxjs';
@Injectable()
export class BankexchangeService {

    constructor(private http: HttpClient,
                private conf: MainConfig) { }

    getRates(url: string): Observable<any> {
        const apiUrl = this.conf.getApiUrl();
        return this.http.get(apiUrl + url);
    }
}
