import {Action} from "@ngrx/store";
import {NbuCurrencyModel} from "../shared/models/NbuCurrencyModel";

export namespace NBU_ACTION{
    export const LOAD_NBURATE = 'LOAD_NBURATE';
}

export class LoadNbuRate implements Action{

    readonly type = NBU_ACTION.LOAD_NBURATE;

    constructor(public payload: NbuCurrencyModel[]){

    }

}

export type NbuAction = LoadNbuRate;