import { Injectable } from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Store} from '@ngrx/store'

import {NbuCurrencyModel} from '../models/NbuCurrencyModel'
import {MainConfig} from '../config/mainConfig'
import {AppState} from '../../redux/app.state'
import {LoadNbuRate} from '../../redux/nbu.action'

@Injectable()
export class NbuService {

  constructor(private http: HttpClient,
              private conf: MainConfig,
              private store: Store<AppState>
              ) { }

  getRates(url: string): Observable<NbuCurrencyModel[]>{
    return this.http.get<NbuCurrencyModel[]>(this.conf.getApiUrl() + url);
  }

  loadRates(): Promise<NbuCurrencyModel[]>{

      return new Promise((resolve, reject) => {

          this.http.get(this.conf.getApiUrl() + this.conf.nbuUrl).subscribe((data: NbuCurrencyModel[]) => {
              this.store.dispatch(new LoadNbuRate(data));
              resolve(data);
          }, error => {
              console.log(error);
              reject(error);
          });
      })


  }

}
