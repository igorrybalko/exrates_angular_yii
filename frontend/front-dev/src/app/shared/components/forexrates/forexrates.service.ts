import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {ForexModel} from "../../models/ForexModel";
import {MainConfig} from "../../config/mainConfig";

@Injectable()
export class ForexratesService {

  constructor(private http: HttpClient,
              private conf: MainConfig) { }

  getRates(url: string): Observable<ForexModel>{
    let apiUrl = this.conf.getApiUrl();
    return this.http.get<ForexModel>(apiUrl + url);
  }

}
