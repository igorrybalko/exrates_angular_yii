'use_strict';
//base part
let gulp = require('gulp'),
    rename  = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    include = require('gulp-include'),
    uglify  = require('gulp-uglify');

//css part
let sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

function swallowError(error){
    console.log(error.toString());
    this.emit('end');
}

gulp.task('default', ['gulp_watch']);

gulp.task('gulp_watch', function () {
    gulp.watch('../../backend/src/scss/**/*.scss', ['styles_admin']);
    gulp.watch('../../backend/src/js/**/*.js', ['scripts_admin']);
});

gulp.task('styles_admin', function () {
    return gulp.src('../../backend/src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 20 versions', '> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../../backend/web/css'));
});

gulp.task('scripts_admin', function() {
    return gulp.src('../../backend/src/js/index.js')
        .pipe(include())
        .pipe(rename('app.min.js'))
        .on('error', swallowError)
        .pipe(uglify()) //минифицируем js файл
        .pipe(gulp.dest('../../backend/web/js'));   //сохраняем минифицированную версию
});