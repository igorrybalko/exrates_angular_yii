<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.min.css',
    ];
    public $js = [
        'runtime.js',
        'polyfills.js',
        'main.js',
        'vendor.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
