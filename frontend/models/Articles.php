<?php

namespace frontend\models;

use common\models\Articles as ArticlesCommon;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $meta_tags
 * @property string $meta_description
 * @property int $published
 * @property string $created
 * @property string $seo_title
 */
class Articles extends ArticlesCommon
{

}
