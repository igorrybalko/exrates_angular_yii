<?php

namespace frontend\models;

use yii\db\Query;

use common\models\Menu as MenuCommon;

class Menu extends MenuCommon
{

    public static function getItems($id)
    {
        $query = new Query();
        $select = ['i.title', 'i.url', 'i.id'];
        $result = $query->
        select($select)->
        from('menus m')->
        innerJoin('menu_items i', 'm.id = i.menu')->
        where([
            'i.published' => 1,
            'm.id' => $id
        ])->orderBy('i.order')->
        all();

        return $result;
    }

}
