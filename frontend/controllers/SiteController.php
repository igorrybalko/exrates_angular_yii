<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\helpers\CacheApiHelper;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $settings = Yii::$app->settings;
        $allowedUrl = $settings->get('common', 'allowedUrl');
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Allow-Headers' => ['Content-Type']
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionApiForex()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('forex.json', 'http://apilayer.net/api/live?access_key=a8dbeee8c4f6a55a44dd50e944cdd832&currencies=EUR,GBP,CHF,JPY,PLN,CNY,RUB&source=USD&format=1');
        return $result;
    }

    public function actionApiTest()
    {
        $post = Yii::$app->request;
        return 'test';
    }
}
