<?php

namespace frontend\controllers;

use frontend\models\Articles;
use frontend\helpers\CacheApiHelper;
use Yii;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

class ArticlesController extends \yii\web\Controller
{
    public function behaviors()
    {
        $settings = Yii::$app->settings;
        $allowedUrl = $settings->get('common', 'allowedUrl');
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => [$allowedUrl],
                    'Access-Control-Request-Method' => ['GET'],
                ],
            ],
        ], parent::behaviors());
    }

    private function getArticle($id)
    {
        $article = Articles::find()->select(['title', 'meta_tags', 'meta_description', 'seo_title'])->where(['id' => $id, 'published' => 1])->asArray()->one();
        Yii::$app->view->params['title'] = $article['seo_title'] ? $article['seo_title'] : $article['title'];
        Yii::$app->view->params['meta_description'] = $article['meta_description'];
        Yii::$app->view->params['meta_tags'] = $article['meta_tags'];
        return $this->render('../site/index');
    }

    public function actionBankExchange()
    {
        return $this->getArticle(5);
    }

    public function actionNbu()
    {
        return $this->getArticle(6);
    }

    public function actionConverter()
    {
        return $this->getArticle(9);
    }

    public function actionCrypto()
    {
        return $this->getArticle(7);
    }

    public function actionNews()
    {
        return $this->getArticle(8);
    }

    public function actionApiBankExchange()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('bankexchange.json', 'http://resources.finance.ua/ru/public/currency-cash.json');
        return $result;
    }

    public function actionApiNbu()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('nbu.json', 'http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json');
        return $result;
    }

    public function actionApiCrypto()
    {
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('crypto.json', 'https://api.coinmarketcap.com/v1/ticker/', 'crypto');
        return $result;
    }

    public function actionApiNews()
    {
        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $cacheApiHelper = new CacheApiHelper();
        $result = $cacheApiHelper->getData('news.json', 'http://k.img.com.ua/rss/ru/financial.xml', 'xml');

        return $result;
    }
}
