<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Menu;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;

class MenuController extends \yii\web\Controller
{
    public function behaviors()
    {
        $settings = Yii::$app->settings;
        $allowedUrl = $settings->get('common', 'allowedUrl');
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => [$allowedUrl],
                    'Access-Control-Request-Method' => ['GET'],
                ],
            ],
        ], parent::behaviors());
    }

    public function actionItems()
    {
        $id = Yii::$app->request->get('id');
        $menuItems = json_encode(Menu::getItems($id));
        return $menuItems;

    }

}
