<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Menu as MenuWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <base href="/">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="w1WQtk9KbKAU6w9eklB_XyRiQXrt7YXbJmq-u3zCuqs" />
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
    <link rel="shortcut icon" href="/images/favicons/favicon.ico">

    <?= Html::csrfMetaTags() ?>
    <meta name="description"  content="<?= Html::encode($this->params['meta_description']) ?>" />
    <meta name="keywords"  content="<?= Html::encode($this->params['meta_tags']) ?>" />
    <title><?= Html::encode($this->params['title']) ?></title>
    <?php $this->head() ?>
    <?php echo MenuWidget::render(1);?>
</head>
<body>
<?php $this->beginBody() ?>
    <app-root></app-root>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
