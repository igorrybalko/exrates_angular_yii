<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menu_items".
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property int $menu
 * @property int $published
 * @property string $created
 */
class MenuItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'menu'], 'required'],
            [['menu', 'published', 'order'], 'integer'],
            [['created'], 'safe'],
            [['title', 'type', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
            'url' => Yii::t('app', 'Url'),
            'menu' => Yii::t('app', 'Menu'),
            'order' => Yii::t('app', 'Order'),
            'published' => Yii::t('app', 'Published'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
}
